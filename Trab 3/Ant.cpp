#include <algorithm>
#include <set>
#include "Data.cpp"
using namespace std;
class Ant {
public:
	Ant(double a,double b, Data* d);
	vector<int> stop();
	void step();
private:
	vector<int> trail;
	set<int> available;
	double alpha;
	double beta;
	Data* d;
	void reset();
	void deposit();
	double getRand();
	double moveProbability(int i,int j,double norm);
	double probabilityNorm(int currentCity);
};

Ant::Ant(double a,double b, Data* d) {
	alpha = a;
	beta = b;
	trail.push_back(1);					
	for(int i=2;i<=d->getN();i++) {
		available.insert(i);
	}
	this->d = d;
}
void Ant::reset() {
	vector<int> L;
	L.push_back(1);
	trail = L;
	for(int i=2;i<=d->getN();i++) {
		available.insert(i);
	}
}
void Ant::deposit() {
	//~ cout << "A" << endl;
	double tourCost = d->tourCost(trail);
	int Q = 500;
	double depositAmount = Q / tourCost;
	int l = trail.size();
	l = l-1;
	for (int i=0;i <l; i++) {
		d->T[trail[i]][trail[i+1]] += depositAmount;
	}
	d->T[trail[l]][trail[0]] += depositAmount;
}

vector<int> Ant::stop() {
	deposit();
	vector<int> temp = trail;
	reset();
	return temp;
}

void Ant::step() {
	int currentCity = trail[trail.size()-1];
	double norm = probabilityNorm(currentCity);
	double p,gp;
	bool moved = false;
	double highestProb = 0;
	double cityHighest = 0;
	for(set<int>::iterator i=available.begin(); i != available.end() ; i++) {
		p = moveProbability(currentCity,*i,norm);
		if (p > highestProb) {
			cityHighest = *i;
			highestProb = p;
		}
		gp = getRand();
		if (gp <= p) {
			moved = true;
			trail.push_back(*i);
			available.erase(i);
			break;
		}
	}
	if(!moved) {
		trail.push_back(cityHighest);
		available.erase(cityHighest);
	}
}

double Ant::getRand() {
		double p = (rand() / (RAND_MAX + 1.0));
	return p;
}
double Ant::moveProbability(int i,int j,double norm) {
	double p = (pow(d->T[i][j],alpha))*(pow(d->visibility[i][j],beta));
	p /= norm;
	return p;
}

double Ant::probabilityNorm(int currentCity) {
	//~ unsigned int size = available.size();
	double norm = 0.0;
	for(set<int>::iterator i=available.begin(); i != available.end() ; i++) {
		norm += (pow(d->T[currentCity][*i],alpha))*(pow(d->visibility[currentCity][*i],beta));
	}
	return norm;
}
