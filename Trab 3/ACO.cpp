#include <fstream>
#include <string.h>
#include <ctime>
#include "Ant.cpp"
//~ #include "Data.cpp"

using namespace std;
class ACO {
public:
	ACO(double a,double b,double e, Data* d);
	double run();
private:
	int N;
	int M;
	vector<Ant> ant;
	double evaporation;
	double alpha;
	double beta;
	Data* d;
};

ACO::ACO(double a,double b,double e, Data* d) {
	alpha = a;
	beta = b;
	evaporation = e;
	N = d->getN();
	M = 3*N;
	for(int i=0; i<M; i++) {
		Ant a(alpha,beta, d);
		ant.push_back(a);
		//~ cout << "formiga criada" << endl;
	}
	this->d = d;
}

double ACO::run() {
	//~ cout << "AQ" << endl;
	vector<int> PATH;
	double minTour,tourC;
	for(int n=0;n < 100; n++) {
		for(int p=0; p<(N-1); p++){
			for(int i=0;i<M;i++) {
				ant[i].step();
			}
		}
		
		//~ cout << "AQ2" << endl;
		for(int i=0;i<M;i++) {
			vector<int> p = ant[i].stop();
			if(!PATH.size()) {
				PATH = p;
				minTour = d->tourCost(p);
				continue;
			}
			tourC = d->tourCost(p);
			if(tourC < minTour) {
				minTour = tourC;
				PATH = p;
			}
		}
		cout << minTour << endl;
		//~ cout << "AQ3" << endl;
		for(int i=1;i<=N;i++) {
			for(int j=1;j<=N;j++) {
				d->T[i][j] *= evaporation;
			}
		}
	}
	cout << minTour << endl;
	d->print(PATH);
	return minTour;
}

vector<string> split(string str, string sep){
    char* cstr=const_cast<char*>(str.c_str());
    char* current;
    vector<std::string> arr;
    current=strtok(cstr,sep.c_str());
    while(current != NULL){
        arr.push_back(current);
        current=strtok(NULL, sep.c_str());
    }
    return arr;
}

int main(){
	ifstream myfile ("pr439.txt");
	
	vector<pair<double,double>> cityCoords;
	int n;
	cityCoords.push_back(make_pair(0,0));
	cout << "Lendo arquivo..." << endl;
	if (myfile.is_open()){
		string line;
		getline(myfile,line);
		n = stoi(line);
		while(getline(myfile,line)){
			double x, y;
			vector<string> arr = split(line," ");
			x = stod(arr[0]);
			y = stod(arr[1]);
			cityCoords.push_back(make_pair(x,y));
		}
		myfile.close();
	}
	cout << "Arquivo lido, calculando resposta..." << endl;
	//Algorithm parameters, changes here may affect the execution
	double alpha = 1;//pheromone importance
	double beta = 3;//visibilitu importance
	double evaporation = 0.85;//pheromone evaporation
	Data d(cityCoords,n);
	ACO colony(alpha,beta,evaporation,&d);
	clock_t begA,endA;
    begA = clock();
	colony.run();
	endA = clock();
	cout << "Tempo gasto Total: " << (double)(endA-begA)/CLOCKS_PER_SEC << " segundos" << endl;
	return 0;
}

