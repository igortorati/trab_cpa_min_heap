#include <vector>
#include <cstdio>
#include <iostream>
#include <cmath>

using namespace std;
class Data {
public:
	Data(vector<pair<double,double>> cityCoords, int n);
	int getN();
	
	void print(vector<int> C);
	double tourCost(vector<int> C);
	vector<vector<double>> T;
	vector<vector<double>> visibility;
private:
	vector<pair<double,double>> cityCoords;
	vector<vector<double>> cost;
	int N;
};
int Data::getN(){
	return N;
}
	
Data::Data(vector<pair<double,double>> cityCoords, int n) {
	N = n;
	this->cityCoords = cityCoords;
	vector< double> M,U,L;
	T.push_back(M);
	visibility.push_back(U);
	cost.push_back(L);
	
	for(int i=0;i<N;i++) {
		vector<double> V(N+1);
		vector<double> t(N+1);
		vector<double> l(N+1);
		for(int j = 1; j<=N ; j++) {
			double x1,x2,y1,y2;
			x1 = cityCoords[i+1].first;
			x2 = cityCoords[j].first;
			y1 = cityCoords[i+1].second;
			y2 = cityCoords[j].second;
			double sum = pow((x2-x1),2.0)+pow((y2-y1),2.0);
			V[j] = sqrt(sum);
			t[j] = 1.0;
			if(V[j] != 0) {
				l[j] = 1/ V[j];
			}
			//~ cout << V[j] << "|(" << x1 << "," <<y1<<")" << "|(" << x2 << "," <<y2<<")" <<endl;
			//~ cout << i << "|" <<j<<":"<<V[j]<<endl;
		}
		cost.push_back(V);
		T.push_back(t);
		visibility.push_back(l);
	}
	//~ cout << "OK2" << endl;
}

void Data::print(vector<int> C) {
	for(unsigned int i=0;i<C.size();i++)
		cout << C[i] << " ";
	cout << endl;
}

double Data::tourCost(vector<int> C) {
	
	double tourCost = 0.0;
	int l = C.size();
	l = l-1;
	for (int i=0;i <l; i++) {
		tourCost += cost[C[i]][C[i+1]];
	}
	tourCost += cost[C[l]][C[0]];
	return tourCost;
}

