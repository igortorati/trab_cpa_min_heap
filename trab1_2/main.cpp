#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include "Heap.cpp"
using namespace std;

int main(){
	ofstream file("out.txt");
    clock_t begA,endA;
    begA = clock();
    double sumAll = 0.00;
    cout << "Usar Heap Comum ou Otimizada?" << endl << "0- Heap Comum." << endl << "1- Heap Otimizada." << endl;
    int optimizedHeap;
    cin >> optimizedHeap;
    cout<< "Usar valores aleatorios ou o pior caso?" << endl << "0- Pior caso." << endl << "1- Valores aleatorios." << endl;
    int opcao;
    cin >> opcao;
    int upperLimit = pow(10,8);
    if(optimizedHeap == 1){
		if(opcao == 0 or opcao == 1){
			for (int i = 1; i< upperLimit; i+=100000) {
			//~ for(int j = 0; j <1000002; ++j){
				Heap heap(i, opcao, optimizedHeap);
				clock_t beg, end;
				beg = clock();
				heap.heapify();
				end = clock();
				if(heap.isHeap() != true) cout << "I: " << i << endl;
				//~ heap.print();
				double iterationElapsedTime = (double)(end-beg)/CLOCKS_PER_SEC;
				sumAll += iterationElapsedTime;
				file << i << "," << iterationElapsedTime <<"\n";
			}
			
			//~ endA = clock();
			//~ cout << "Tempo gasto Total: " << (double)(endA-begA)/CLOCKS_PER_SEC << " segundos" << endl;
			//~ cout << "Tempo gasto no heapify: " << sumAll << " segundos" << endl;
		}else{
			cout << "Opcao inválida!";
		}
	}else if(optimizedHeap == 0){
		if(opcao == 0 or opcao == 1){
			for (int i = 1; i< upperLimit; i+=100000) {
			//~ for(int j = 0; j <1000002; ++j){
				clock_t beg, end;
				beg = clock();
				Heap heap(i, opcao, optimizedHeap);
				//~ heap.heapify();
				end = clock();
				if(heap.isHeap() != true) cout << "I: " << i << endl;
				//~ heap.print();
				double iterationElapsedTime = (double)(end-beg)/CLOCKS_PER_SEC;
				sumAll += iterationElapsedTime;
				file << i << "," << iterationElapsedTime <<"\n";
			}
			
			//~ endA = clock();
			//~ cout << "Tempo gasto Total: " << (double)(endA-begA)/CLOCKS_PER_SEC << " segundos" << endl;
			//~ cout << "Tempo gasto no heapify: " << sumAll << " segundos" << endl;
		}else{
			cout << "Opcao inválida!";
		}
	}else{
		cout << "Opcao inválida!";
	}
	endA = clock();
		cout << "Tempo gasto Total: " << (double)(endA-begA)/CLOCKS_PER_SEC << " segundos" << endl;
		cout << "Tempo gasto no heapify: " << sumAll << " segundos" << endl;
    /*cout << "Elapsed time in nanoseconds : " 
		<< chrono::duration_cast<chrono::nanoseconds>(end - start).count()
		<< " ns" << endl;

	cout << "Elapsed time in microseconds : " 
		<< chrono::duration_cast<chrono::microseconds>(end - start).count()
		<< " µs" << endl;*/
	return 0;
}

