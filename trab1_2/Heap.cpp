#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
using namespace std;

class Heap{
	private:
		int* array;
		int size;
		int findFather(int pos);
		void commomHeapWorst();
		void commomHeapRandom();
	public:
		Heap(int size, bool randomized, bool optimizedHeap);
		
		~Heap();
		void heapify();
		bool isHeap();
		
		void commomHeap(int v, int pos);
};

Heap::Heap(int size, bool randomized, bool optimizedHeap){
	this->size = size;
	array = new int[size];
	if(optimizedHeap){
		if(randomized){
			//~ cout << "Modo Aleatório." << endl;
			srand(time(0));
			for(int i = 0; i < size; ++i){
				array[i] = rand();
			}
		}else{
			//~ cout << "Modo Pior Caso." << endl;
			int value = 1;
			for(int i = size-1; i >= 0; --i){
				array[i] = value;
				++value;
			}
		}
	}else{
		if(randomized){
			
			commomHeapRandom();
		}else{
			commomHeapWorst();
		}
	}
	cout << "AAA";
	//~ cout << array[0] << "|" << array[1] << "|" << array[2] << endl;
}

Heap::~Heap(){
	delete[] array;
}

void Heap::heapify(){
	int k, j, v;
	
	for (int i = (floor(size/2.0f)); i >= 0; --i){
		k = i;
		v = array[k];
		bool sorted = false;
		while(!sorted and 2*k<size){
			j = 2*k+1;
			if(j<size-1){//define se tem dois filhos
				if(array[j] > array[j+1]) j = j+1;
			}
			if(v <= array[j]) sorted = true;
			else{
				array[k] = array[j];
				k = j;
			}
		}
		array[k] = v;
	}
	
}

bool Heap::isHeap(){
	for (int i = 0; i <= (size - 2) / 2; i++) {
		if (array[i] > array[2 * i + 1] ||
			((2 * i + 2 < size) && array[i] > array[2 * i + 2])) {
			return false;
		}
	}
	return true;
}

int Heap::findFather(int pos){
	if(pos % 2 == 0)
		return (pos/2) -1;
	return pos/2;
}

void Heap::commomHeapRandom(){
	srand(time(0));
	int value = size-1;
	for(int j = 0; j < size; ++j){
		array[j] = rand();
		int i = j;
		int f = findFather(i);
		while(i >= 1 and array[f] > array[i]){
			swap(array[f], array[i]);
			i = f;
			f = findFather(i);
		}
		--value;
	}
	
}

void Heap::commomHeapWorst(){
	int value = size-1;
	for(int j = 0; j < size; ++j){
		array[j] = value;
		int i = j;
		int f = findFather(i);
		while(i >= 1 and array[f] > array[i]){
			swap(array[f], array[i]);
			i = f;
			f = findFather(i);
		}
		--value;
	}
	
}
